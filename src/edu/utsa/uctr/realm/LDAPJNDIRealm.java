/*
 *     This file is part of ldap-jndi-jass-realm
 *
 * ldap-jndi-jass-realm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ldap-jndi-jass-realm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.utsa.uctr.realm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;

import javax.sql.DataSource;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.security.auth.login.LoginException;

import com.sun.enterprise.security.auth.realm.BadRealmException;
import com.sun.enterprise.security.auth.realm.NoSuchUserException;
import com.sun.enterprise.security.auth.realm.NoSuchRealmException;
import com.sun.enterprise.security.auth.realm.InvalidOperationException;
import com.sun.enterprise.security.auth.realm.IASRealm;

import java.security.MessageDigest;
import java.util.List;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.StartTlsResponse;

import org.jvnet.hk2.annotations.Service;

/**
 * Realm wrapper for LDAP authentication + JNDI authorization.
 *
 * <P>See LDAPJNDILoginModuule documentation for more details on the operation
 * of the LDAP+JNDI realm and login module.
 *
 * <P>The ldap realm needs the following properties in its configuration:
 * <ul>
 *   <li>directory - URL of LDAP directory to use
 *   <li>base-dn - The base DN to use for user searches.
 *   <li>jaas-ctx - JAAS context name used to access LoginModule for
 *       authentication.
 * </ul>
 *
 * <P>Besides JDK Context properties start with java.naming, javax.security,
 *  one can also set connection pool related properties starting with
 *  com.sun.jndi.ldap.connect.pool.
 *  See http://java.sun.com/products/jndi/tutorial/ldap/connect/config.html
 *  for details.
 *  Also, the following optional attributes can also be specified:
 * <ul>
 *   <li>search-filter - LDAP filter to use for searching for the user
 *       entry based on username given to iAS. The default value is
 *       <code>uid=%s</code> where %s is expanded to the username.
 *   <li>group-base-dn - The base DN to use for group searches. By default
 *       its value is the same as base-dn.
 *   <li>group-search-filter - The LDAP filter to use for searching group
 *       membership of a given user. The default value is <code>
 *       uniquemember=%d</code> where %d is expanded to the DN of the
 *       user found by the user search.
 *   <li>group-target - The attribute which value(s) are interpreted as
 *       group membership names of the user. Default value is <code>cn</code>.
 *   <li>search-bind-dn - The dn of ldap user. optional and no default value.
 *   <li>search-bind-password - The password of search-bind-dn.optional and 
 *       no default value.
 *   <li>pool-size - The JNDI ldap connection pool size.
 * </ul>
 * 
 * @see com.sun.enterprise.security.auth.login.LDAPLoginModule
 *
 */
@Service
public final class LDAPJNDIRealm extends IASRealm
{
    // Descriptive string of the authentication type of this realm.
    public static final String AUTH_TYPE = "hybrid";

    // These are property names which should be in auth-realm in server.xml
    public static final String PARAM_DIRURL="directory";
    public static final String PARAM_USERDN="base-dn";
    
    // These are optional, defaults are provided
    // %s = subject name
    // %d = DN of user search result
    public static final String PARAM_SEARCH_FILTER="search-filter";
    public static final String PARAM_MODE="mode";
    public static final String PARAM_JNDICF="jndiCtxFactory";
    public static final String PARAM_POOLSIZE="pool-size";
    
    // These are optional, no default values are provided
    //public static final String PARAM_BINDDN="search-bind-dn";
    //public static final String PARAM_BINDPWD="search-bind-password";

    // Only find-bind mode is supported so mode attribute is not exposed yet
    public static final String MODE_FIND_BIND="find-bind";

    // Expansion strings
    public static final String SUBST_SUBJECT_NAME="%s";
    public static final String SUBST_SUBJECT_DN="%d";

    // Defaults
    private static final String SEARCH_FILTER_DEFAULT=
                                     "uid="+SUBST_SUBJECT_NAME;
    private static final String MODE_DEFAULT=MODE_FIND_BIND;
    private static final String JNDICF_DEFAULT=
                                     "com.sun.jndi.ldap.LdapCtxFactory";
    private static final int POOLSIZE_DEFAULT=5;

    private final String[] _dnOnly = {"dn"};
    
    private static final String SUN_JNDI_POOL = "com.sun.jndi.ldap.connect.pool";
    private static final String SUN_JNDI_POOL_ = "com.sun.jndi.ldap.connect.pool.";
    private static final String SUN_JNDI_POOL_PROTOCOL = "com.sun.jndi.ldap.connect.pool.protocol";
    private static final String SUN_JNDI_POOL_MAXSIZE = "com.sun.jndi.ldap.connect.pool.maxsize";
    // dynamic group related properties.
    private static final String DYNAMIC_GROUP_OBJECT_FACTORY = "com.sun.jndi.ldap.obj.LdapGroupFactory";
    public static final String DYNAMIC_GROUP_FACTORY_OBJECT_PROPERTY = "java.naming.factory.object";
    private static final String DYNAMIC_GROUP_STATE_FACTORY = "com.sun.jndi.ldap.obj.LdapGroupFactory";
    public static final String DYNAMIC_GROUP_STATE_FACTORY_PROPERTY = "java.naming.factory.state";

    public static final String LDAP_SOCKET_FACTORY = "java.naming.ldap.factory.socket";
    public static final String DEFAULT_SSL_LDAP_SOCKET_FACTORY = "com.sun.enterprise.security.auth.realm.ldap.CustomSocketFactory";
    public static final String LDAPS_URL = "ldaps://";
    public static final String DEFAULT_POOL_PROTOCOL = "plain ssl";
    
    public static final String SSL = "SSL";

    // Required JDBC properties
    public static final String PARAM_DATASOURCE_JNDI = "datasource-jndi";
    //public static final String PARAM_DB_USER = "db-user";
    //public static final String PARAM_DB_PASSWORD = "db-password";

    public static final String PARAM_CHARSET = "charset";
    public static final String PARAM_USER_TABLE = "user-table";
    public static final String PARAM_ROLE_COLUMN = "role-column";
    public static final String PARAM_USER_NAME_COLUMN = "user-name-column";

    private static final char[] HEXADECIMAL = { '0', '1', '2', '3',
        '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    private Map<String, Vector> groupCache;
    private Vector<String> emptyVector;
    private String roleQuery = null;
    private MessageDigest md = null;

    private Properties ldapBindProps = new Properties();

    /**
     * Initialize a realm with some properties.  This can be used
     * when instantiating realms from their descriptions.  This
     * method may only be called a single time.  
     *
     * @param props Initialization parameters used by this realm.
     * @exception BadRealmException If the configuration parameters
     *     identify a corrupt realm.
     * @exception NoSuchRealmException If the configuration parameters
     *     specify a realm which doesn't exist.
     *
     */
    @Override
    public synchronized void init(Properties props) throws BadRealmException, NoSuchRealmException
    {
        super.init(props);

        String url = props.getProperty(PARAM_DIRURL);
        this.setProperty(PARAM_DIRURL, url);
        ldapBindProps.setProperty(Context.PROVIDER_URL, url);
        
        String dn = props.getProperty(PARAM_USERDN);
        this.setProperty(PARAM_USERDN, dn);

        String jaasCtx = props.getProperty(IASRealm.JAAS_CONTEXT_PARAM);
        this.setProperty(IASRealm.JAAS_CONTEXT_PARAM, jaasCtx);
        
        if (url==null || dn==null || jaasCtx==null) {
            String msg = "ldaprealm.badconfig" + url + dn +jaasCtx;
            throw new BadRealmException(msg);
        }

        String mode = props.getProperty(PARAM_MODE, MODE_DEFAULT);
        if (!MODE_DEFAULT.equals(mode)) {
            String msg = "ldaprealm.badmode" + mode;
            throw new BadRealmException(msg);
        }
        this.setProperty(PARAM_MODE, mode);

        String ctxF = props.getProperty(PARAM_JNDICF, JNDICF_DEFAULT);
        this.setProperty(PARAM_JNDICF, ctxF);
        ldapBindProps.setProperty(Context.INITIAL_CONTEXT_FACTORY, ctxF);

        String searchFilter = props.getProperty(PARAM_SEARCH_FILTER, SEARCH_FILTER_DEFAULT);
        this.setProperty(PARAM_SEARCH_FILTER,searchFilter);

        String charset = props.getProperty(PARAM_CHARSET);
        if (charset != null) {
            this.setProperty(PARAM_CHARSET, charset);
        }

        String dsJndi = props.getProperty(PARAM_DATASOURCE_JNDI);
        if (dsJndi == null) {
            String msg = "realm.missingprop " + PARAM_DATASOURCE_JNDI + " LDAPJNDIRealm";
            throw new BadRealmException(msg);
        }
        this.setProperty(PARAM_DATASOURCE_JNDI, dsJndi);

        String userTable = props.getProperty(PARAM_USER_TABLE);
        if (userTable == null) {
            String msg = "realm.missingprop " + PARAM_USER_TABLE + " LDPAJNDIRealm";
            throw new BadRealmException(msg);
        }

        String userNameColumn = props.getProperty(PARAM_USER_NAME_COLUMN);
        if (userNameColumn == null) {
            String msg = "realm.missingprop " + PARAM_USER_NAME_COLUMN + " LDAPJNDIRealm";
            throw new BadRealmException(msg);
        }

        String roleColumn = props.getProperty(PARAM_ROLE_COLUMN);
        if (roleColumn == null) {
            String msg = "realm.missingprop " + PARAM_ROLE_COLUMN + " LDAPJNDIRealm";
            throw new BadRealmException(msg);
        }
        
        roleQuery = "SELECT " + userNameColumn + ", " + roleColumn + " FROM " +
                userTable + " WHERE " + userNameColumn + " = ? ";

        groupCache = new HashMap<String, Vector>();
        emptyVector = new Vector<String>();

        Enumeration penum = props.propertyNames();
        while (penum.hasMoreElements()) {
            String propName = (String)penum.nextElement();
            if (propName.startsWith("java.naming.") ||
                    propName.startsWith("javax.security.")) {
                ldapBindProps.setProperty(propName, props.getProperty(propName));
            } else if (propName.startsWith(SUN_JNDI_POOL_) &&
                    !SUN_JNDI_POOL_MAXSIZE.equals(propName)) {
                if (System.getProperty(propName) == null) {
                    System.setProperty(propName, props.getProperty(propName));
                }
            }
        }

        String poolSize = Integer.getInteger(PARAM_POOLSIZE,POOLSIZE_DEFAULT).toString();
        String sunPoolSizeStr = props.getProperty(SUN_JNDI_POOL_MAXSIZE, poolSize);
        //Precedence rule: SUN_JNDI_POOL_MAXSIZE > PARAM_POOLSIZE > POOLSIZE_DEFAULT
        try {
            sunPoolSizeStr = Integer.valueOf(sunPoolSizeStr).toString();
        } catch(Exception ex) {
            sunPoolSizeStr = poolSize;
        }
        if (System.getProperty(SUN_JNDI_POOL_MAXSIZE) == null) {       
            System.setProperty(SUN_JNDI_POOL_MAXSIZE, sunPoolSizeStr);
        }
        this.setProperty(PARAM_POOLSIZE, sunPoolSizeStr);

        String usePool = props.getProperty(SUN_JNDI_POOL, "true");
        ldapBindProps.setProperty(SUN_JNDI_POOL, usePool);

        if(url != null && url.startsWith(LDAPS_URL)) {
            ldapBindProps.setProperty(LDAP_SOCKET_FACTORY, DEFAULT_SSL_LDAP_SOCKET_FACTORY);
            if (System.getProperty(SUN_JNDI_POOL_PROTOCOL) == null) {
                System.setProperty(SUN_JNDI_POOL_PROTOCOL, DEFAULT_POOL_PROTOCOL);
                if (_logger.isLoggable(Level.FINE)) {
                    _logger.log(Level.FINE, "LDAPJNDIRealm : Using custom socket factory for SSL with pooling");
                }
            }
        } else {
            if (_logger.isLoggable(Level.FINE)) {
                _logger.log(Level.WARNING, "LDAPJNDIRealm : No SSL?");
            }
        }

        groupCache = new HashMap();
        emptyVector = new Vector();
    }


    /**
     * Returns a short (preferably less than fifteen characters) description
     * of the kind of authentication which is supported by this realm.
     *
     * @return Description of the kind of authentication that is directly
     *     supported by this realm.
     */
    public String getAuthType()
    {
        return AUTH_TYPE;
    }
    
    /**
     * Get binding properties defined in server.xml for LDAP server.
     *
     */
    private Properties getLdapBindProps()
    {
        return (Properties)ldapBindProps.clone();
    }


    /**
     * Returns the name of all the groups that this user belongs to.
     * Note that this information is only known after the user has
     * logged in. This is called from web path role verification, though
     * it should not be.
     *
     * @param username Name of the user in this realm whose group listing
     *     is needed.
     * @return Enumeration of group names (strings).
     * @exception InvalidOperationException thrown if the realm does not
     *     support this operation - e.g. Certificate realm does not support
     *     this operation.
     */
    public Enumeration getGroupNames (String username)
        throws InvalidOperationException, NoSuchUserException
    {
        Vector v = (Vector)groupCache.get(username);
        if (v == null) {
            if (_logger.isLoggable(Level.FINE)) {
                _logger.log(Level.FINE, "No groups available for: {0}", username);
            }
            // we don't load group here as we need to bind ctx to user with
            // password before doing that and password is not available here
            return emptyVector.elements();
        } else {
            return v.elements();
        }
    }


    /**
     * Set group membership info for a user.
     *
     * <P>See bugs 4646133,4646270 on why this is here.
     *
     */
    private void setGroupNames(String username, String[] groups)
    {
        Vector v = new Vector(groups.length);
        v.addAll(Arrays.asList(groups));
        groupCache.put(username, v);
    }

    /**
     * Supports mode=find-bind. See class documentation.
     *
     */
    public String[] findAndBind(String _username, String _password)
        throws LoginException
    {
        _username = _username.toLowerCase();
        String userBindDN = _username + "@utsarr.net";
        StringBuffer sb = new StringBuffer(getProperty(PARAM_SEARCH_FILTER));
        substitute(sb, SUBST_SUBJECT_NAME, _username);
        String userFilter = sb.toString();
        // attempt to bind as the user
        //DirContext ctx = null;
        //String srcFilter = null;
        String[] grpList = null;
        ArrayList groupsList = new ArrayList();
        //try {
            boolean bindSuccessful = bindAsUser(userBindDN, _password, userFilter);
            if (bindSuccessful == false) {
                String msg = "ldaprealm.bindfailed " + _username;
                _logger.log(Level.INFO, "Authentication unsuccessful for: " + _username);
                throw new LoginException(msg);
            }
            // search for group memberships
            Connection groupConn = getConnection();
            assignGroups(groupConn, _username, groupsList);
            close(groupConn, null, null);
            // search groups using above connection, substituting %d (and %s)
            grpList = new String[groupsList.size()];
            groupsList.toArray(grpList);
            _logger.info(groupsList.get(0).toString());
        //} catch (Exception e) {
        //    throw new LoginException(e.toString());
        //} finally {
        //}

        grpList = addAssignGroups(grpList);
        setGroupNames(_username, grpList);

        if(_logger.isLoggable(Level.FINE)){
             _logger.log(Level.FINE, "LDAPJNDIRealm: login succeeded for: {0}", _username);
        }

        return grpList;
    }

    /**
     * Do anonymous search for the user. Should be unique if exists.
     *
     */
    private String userSearch(DirContext ctx, String baseDN, String filter) throws NamingException
    {
        //if (_logger.isLoggable(Level.FINEST)) {
            _logger.log(Level.INFO, "search: baseDN: {0}  filter: {1}", new Object[]{baseDN, filter});
        //}
            
        String foundDN = null;
        NamingEnumeration namingEnum = null;

        SearchControls ctls = new SearchControls();
        ctls.setReturningAttributes(_dnOnly);
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        ctls.setCountLimit(1);

            namingEnum = ctx.search(baseDN, filter, ctls);
            if (namingEnum.hasMore()) {
                SearchResult res = (SearchResult)namingEnum.next();

                StringBuilder sb = new StringBuilder();
                //for dn name with '/'
                CompositeName compDN = new CompositeName(res.getName());
                String ldapDN = compDN.get(0);
                sb.append(ldapDN);
                
                if (res.isRelative()) {
                    sb.append(",");
                    sb.append(baseDN);
                }
                foundDN = sb.toString();
                _logger.log(Level.INFO, "Found user DN: {0}", foundDN);
            }

        return foundDN;
    }

    /**
     * Attempt to bind as a specific DN.
     *
     */
    private boolean bindAsUser(String bindDN, String password, String userFilter)
    {
        boolean bindSuccessful=false;

        Properties p = getLdapBindProps();
        StartTlsResponse tls = null;
        InitialLdapContext ctx = null;
        String userSearchResult = null;

        _logger.info(p.toString());
        try {
            ctx = new InitialLdapContext(p, null);
            //tls = (StartTlsResponse) ctx.extendedOperation(new StartTlsRequest());
            //SSLSession sess = tls.negotiate();
            //_logger.log(Level.INFO, "Negotiated StartTLS session: {0}", sess.toString());
            ctx.addToEnvironment(Context.SECURITY_AUTHENTICATION, "simple");
            ctx.addToEnvironment(Context.SECURITY_PRINCIPAL, bindDN);
            ctx.addToEnvironment(Context.SECURITY_CREDENTIALS, password);
            userSearchResult = userSearch(ctx, getProperty(PARAM_USERDN), userFilter);
            _logger.log(Level.INFO, "User Search Result: {0}", userSearchResult);
            bindSuccessful = true;
        } catch (Exception e) {
                _logger.log(Level.SEVERE, "Error binding to directory as: {0}", bindDN);
                _logger.log(Level.SEVERE, "Exception from JNDI: {0}", e.toString());
        } finally {
            if (ctx != null) {
                try {
                    //tls.close();
                    ctx.close();
                } catch (Exception e) {};
            }
        }
        return bindSuccessful;
    }

    /**
     * Do string substitution. target is replaced by value for all
     * occurences.
     *
     */
    private static void substitute(StringBuffer sb, String target, String value)
    {
        int i = sb.indexOf(target);
        while (i >= 0) {
            sb.replace(i, i+target.length(), value);
            i = sb.indexOf(target);
        }
    }

    private boolean assignGroups(Connection conn, String _username, List groupList) {
        boolean pass = false;
        PreparedStatement ps = null;
        try {
            ps  = conn.prepareStatement(roleQuery);
            ps.setString(1, _username);
            ResultSet rs = ps.executeQuery();
            _logger.info("Searching for group memberships:");
            if (rs.next()) {
                String roleName = (String) rs.getString(2);
                _logger.log(Level.WARNING, "Roles found for: {0}", _username);
                _logger.log(Level.WARNING, "Attaching role: {0}", roleName);
                groupList.add(roleName);
            } else {
                _logger.log(Level.WARNING, "Found 0 roles for user: {0}", _username);
            }
        } catch (Exception e) {

        }
        return pass;
    }

    private void close(Connection conn, PreparedStatement stmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch(Exception ex) {
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch(Exception ex) {
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch(Exception ex) {
            }
        }
    }

    /**
     * Return a connection from the properties configured
     * @return a connection
     */
    private Connection getConnection() throws LoginException {
        final String dsJndi = this.getProperty(PARAM_DATASOURCE_JNDI);
        try{
            _logger.log(Level.WARNING, "Attempting datasource lookup: {0}", dsJndi);
            InitialContext ctx = new InitialContext();
            final DataSource dataSource = (DataSource) ctx.lookup(dsJndi);
            _logger.log(Level.WARNING, "Attempting getConnection");
            Connection connection = dataSource.getConnection();
            return connection;
        } catch(Exception ex) {
            String msg = sm.getString("jdbcrealm.cantconnect: ", dsJndi);
            LoginException loginEx = new LoginException(msg);
            loginEx.initCause(ex);
            throw loginEx;
        }
    }

}
