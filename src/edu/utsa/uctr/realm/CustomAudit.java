/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.utsa.uctr.realm;

import com.sun.appserv.security.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.appserv.security.AuditModule;
import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author john.garza@utsa.edu
 */
public class CustomAudit extends AuditModule {
    private static final Logger log = Logger.getLogger("edu.utsa.uctr.realm.CustomAudit");

    //private String logFileName = null;
    //private String logFilePath = null;
    //private final String logFileNameProperty = "LogFileName";
    //private final String logFilePathProperty = "LogFilePath";

    // Audit module defined methods
    /*
      Initialization of module with properties defined in the domain.xml.
      This method is invoked during server startup when the audit module
      is initially loaded. The props argument contains the properties defined
      for this module in domain.xml. The module can do any initialization it needs in this
      method. If the method return without throwing an exception S1AS will
      assume the module realm is ready to service audit requests. If an exception
      is thrown the module is disabled.
     */
    @Override
    public void init(Properties props){
        log("init() invoked...");
        //Get logfilename, location properties
        /*
        logFileName = props.getProperty(logFileNameProperty);
        logFilePath = props.getProperty(logFilePathProperty);
        log(logFilePath + "/" + logFileName);
        if ((logFileName==null)||(logFilePath==null)){
            log("Error::Failed in Audit module initialization!");
	    log("LogFileName and LogFilePath Properties are null!");
        }*/
    }
    /*
    This method is invoked when an authentication request has been processed
    by a realm for the given user. The success flag indicates whether the
    authentication was successful or not.
    */
    @Override
    public void authentication(String user, String realm, boolean status){
        //logFile("authentication("+user+","+realm+","+status+") invoked...");
        log("authentication("+user+","+realm+","+status+") invoked...");

    }

    /*
	This method is invoked when a web container call has been processed by
authorization. The success flag indicated whether the authorization was
granted or denied. The req object is the standard HttpServletRequest object
for this request. The type string is one of "hasUserDataPermission",
"hasRoleRefPermission" or "hasResourcePermission" (see JSR-115).
     */
    @Override
    public void webInvocation(String user, HttpServletRequest req, String type, boolean status){
        //logFile("webInvocation("+user+","+req.getRequestURI()+","+type+","+status+") invoked...");
        //logFile("webInvocation("+user+","+req.getAuthType()+","+req.getRemoteUser()+","+type+","+status+") invoked...");
        //log("webInvocation("+user+","+req.getRequestURI()+","+type+","+status+") invoked...");
        //log("webInvocation("+user+","+req.getAuthType()+","+req.getRemoteUser()+","+type+","+status+") invoked...");
    }

    /*
    This method is invoked when an EJB container call has been processed by
    authorization. The success flag indicates whether the authorization was
    granted or denied. The ejb and method strings describe the EJB and its
    method which is being invoked.
     */
    @Override
    public void ejbInvocation(String user, String ejbName, String methodName,
                              boolean status){
        //logFile("ejbInvocation("+user+","+ejbName+","+methodName+","+status+") invoked...");
        if (methodName.contains("Clock")) {
            log("ejbInvocation("+user+","+ejbName+","+methodName+","+status+") invoked...");
        }
    }

    /*
     * This method is invoked during the appserver shutdown.
     * Record a string and stop the server to see whether this method is invoked or not.
     */
    /* Commented the module as it is not exposed and not working.
     Tried before as document listed this.
    public void shutdown(){
        logFile("shutdown()...Test Message before appserver down");
        log("shutdown()...Invoked before appserver down");
    }
    */

    /**
     * Invoked upon completion of the server startup
     */
    @Override
    public void serverStarted() {
        //logFile("serverStarted()...invoked after server started");
        //log("serverStarted()...invoked after server started ");
    }

    /**
     * Invoked upon completion of the server shutdown
     */
    @Override
    public void serverShutdown() {
        //logFile("serverShutdown()...invoked after server shutdown");
        //log("serverShutdown()...invoked after server shutdown");
    }



    /**
     * Invoked during validation of the web service request
     * @param uri The URL representation of the web service endpoint
     * @param endpoint The name of the endpoint representation
     * @param success the status of the web service request validation
     */
    @Override
    public void webServiceInvocation(String uri, String endpoint, boolean success) {
       //logFile("webServiceInvocation("+uri+","+endpoint+","+success+") invoked...");
       log("webServiceInvocation("+uri+","+endpoint+","+success+") invoked...");
    }

    /**
     * Invoked during validation of the web service request
     * @param endpoint The representation of the web service endpoint
     * @param success the status of the web service request validation
     */
    @Override
    public void ejbAsWebServiceInvocation(String endpoint, boolean success) {
       //logFile("ejbAsWebServiceInvocation("+endpoint+","+success+") invoked...");
       log("ejbAsWebServiceInvocation("+endpoint+","+success+") invoked...");
    }


    //------helper methods
    public void log(String mesg){
        log.log(Level.WARNING, "CustomAudit:: {0}", mesg);
        //System.out.println("CustomAudit::"+mesg);
    }

    /*
    public void logFile(String mesg){
	FileOutputStream fout = null;
	try{
          System.out.println("writing to file:"+logFilePath+File.separator+logFileName);
	  fout = new FileOutputStream(new File(logFilePath+File.separator+logFileName),true);
	  PrintWriter out = new PrintWriter(fout);
	  out.println("CustomAudit::"+mesg);
	  out.flush();
	}catch(Exception ex){
	  ex.printStackTrace();
	}finally{
	  try{
	 	if (fout!=null) fout.close();
	 }catch(Exception e){
	   e.printStackTrace();
	 }
	}
    }*/
}

