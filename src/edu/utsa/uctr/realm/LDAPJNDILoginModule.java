/*
 *     This file is part of ldap-jndi-jass-realm
 *
 * ldap-jndi-jass-realm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * ldap-jndi-jass-realm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.utsa.uctr.realm;

import com.sun.enterprise.security.auth.login.PasswordLoginModule;
import javax.security.auth.login.*;
import javax.security.auth.login.LoginException;
/**
 * iAS JAAS LoginModule for an LDAP + JNDI Realm.
 *
 * <P>Used the LDAPLoginModule as a starting base for this code.
 * Often times, in large organizations, a quick prototype of an app
 * may be needed to be built using SSO with a LDAP but with custom
 * role management held in the database.
 *
 * <P>In a university setting, not all users are first class LDAP citizens
 * (i.e.- faculty/staff vs. temporary student accounts), an app wanting
 * to utilize LDAP for authentication is feasible but not when it comes to
 * supporting authorization as well.  For smaller decentralized departments
 * authorization may be easier handled by the same database storing the app's
 * data.  Thus, a hybrid module for JAAS in which LDAP handles authentication
 * while data provided by the rdbm is used to handle authorization is needed.
 *
 * <P>There are various ways in which a user can be authenticated using
 * an LDAP directory. Currently this login module only supports one mode,
 * 'find and bind'. Other modes may be added as schedules permit.
 *
 * <P>Mode: <i>find-bind</i>
 * <ol>
 *  <li>An LDAP search is issued on the directory starting at base-dn
 *      with the given search-filter (having substituted the user name
 *      in place of %s). If no entries match this search, login fails
 *      and authentication is over.
 *  <li>The DN of the entry which matched the search as the DN
 *      of the user in the directory. If the search-filter
 *      is properly set there should always be a single match; if there are
 *      multiple matches, the first one found is used.
 *  <li>Next an LDAP bind is attempted using the above DN and the provided
 *      password. If this fails, login is considered to have failed and
 *      authentication is over.
 *  <li>Then a jndi query is built on the user name to search for any role
 *      memberships which are added to the group membership of the authenticated
 *      user.
 * </ol>
 *
 */
public class LDAPJNDILoginModule extends PasswordLoginModule {

    private LDAPJNDIRealm _realm;

    
    /**
     * Performs authentication for the current user.
     *
     */
    protected void authenticate() throws LoginException {
        if (!(_currentRealm instanceof LDAPJNDIRealm)) {
            throw new LoginException("Realm is not an instance of LDAPJNDIRealm");
        }
        _realm = (LDAPJNDIRealm) _currentRealm;

        // enforce that password cannot be empty.
        // ldap may grant login on empty password!
        if (_password == null || _password.length() == 0) {
            throw new LoginException("Password cannot be empty.");
        }

        String mode = _currentRealm.getProperty(LDAPJNDIRealm.PARAM_MODE);

        if (LDAPJNDIRealm.MODE_FIND_BIND.equals(mode)) {
            String[] grpList = _realm.findAndBind(_username, _password);
            String[] groupListToForward = new String[grpList.length];
            System.arraycopy(grpList, 0, groupListToForward, 0, grpList.length);
            commitAuthentication(_username, _password, _currentRealm, groupListToForward);
        } else {
            String msg = sm.getString("ldaplm.badmode", mode);
            throw new LoginException(msg);
        }
    }
}
